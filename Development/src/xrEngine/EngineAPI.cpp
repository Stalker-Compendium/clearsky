// EngineAPI.cpp: implementation of the CEngineAPI class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "EngineAPI.h"
#include "xrXRC.h"

extern xr_token* vid_quality_token;

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

void __cdecl dummy(void)	{
};
CEngineAPI::CEngineAPI()
{
	hGame = 0;
	hRender = 0;
	hTuner = 0;
	pCreate = 0;
	pDestroy = 0;
	tune_pause = dummy;
	tune_resume = dummy;
}

CEngineAPI::~CEngineAPI()
{
	// destroy quality token here
	if (vid_quality_token)
	{
		for (int i = 0; vid_quality_token[i].name; i++)
		{
			xr_free(vid_quality_token[i].name);
		}
		xr_free(vid_quality_token);
		vid_quality_token = NULL;
	}
}
extern u32 renderer_value; //con cmd

void CEngineAPI::Initialize(void)
{
	//////////////////////////////////////////////////////////////////////////
	// render

	LPCSTR			r3_name = "xrRender.dll";

	{
		// try to initialize R3
		Log("Loading DLL:", r3_name);
		hRender = LoadLibrary(r3_name);
		if (0 == hRender)
			Msg("! ...Failed - incompatible hardware/pre-Vista OS.");
	}

	Device.ConnectToRender();

	// game	
	{
		LPCSTR			g_name = "xrGame.dll";
		Log("Loading DLL:", g_name);
		hGame = LoadLibrary(g_name);
		if (0 == hGame)	R_CHK(GetLastError());
		R_ASSERT2(hGame, "Game DLL raised exception during loading or there is no game DLL at all");
		pCreate = (Factory_Create*)GetProcAddress(hGame, "xrFactory_Create");	R_ASSERT(pCreate);
		pDestroy = (Factory_Destroy*)GetProcAddress(hGame, "xrFactory_Destroy");	R_ASSERT(pDestroy);
	}

	//////////////////////////////////////////////////////////////////////////
	// vTune
	tune_enabled = FALSE;
	if (strstr(Core.Params, "-tune"))	{
		LPCSTR			g_name = "vTuneAPI.dll";
		Log("Loading DLL:", g_name);
		hTuner = LoadLibrary(g_name);
		if (0 == hTuner)	R_CHK(GetLastError());
		R_ASSERT2(hTuner, "Intel vTune is not installed");
		tune_enabled = TRUE;
		tune_pause = (VTPause*)GetProcAddress(hTuner, "VTPause");	R_ASSERT(tune_pause);
		tune_resume = (VTResume*)GetProcAddress(hTuner, "VTResume");	R_ASSERT(tune_resume);
	}
}

void CEngineAPI::Destroy(void)
{
	if (hGame)				{ FreeLibrary(hGame);	hGame = 0; }
	if (hRender)			{ FreeLibrary(hRender); hRender = 0; }
	pCreate = 0;
	pDestroy = 0;
	Engine.Event._destroy();
	XRC.r_clear_compact();
}

void CEngineAPI::CreateRendererList()
{
	//	TODO: ask renderers if they are supported!
	if (vid_quality_token != NULL)		return;

	LPCSTR r3_name = "xrRender.dll";
	
	{
		// try to initialize R3
		Msg("[CEngineAPI::CreateRendererList]Loading DLL:", r3_name);
		//	Hide "d3d10.dll not found" message box for XP
		SetErrorMode(SEM_FAILCRITICALERRORS);
		hRender = LoadLibrary(r3_name);
		//	Restore error handling
		SetErrorMode(0);
		if (hRender)
			FreeLibrary(hRender);
	}

	hRender = 0;

	xr_vector<LPCSTR>			_tmp;

	_tmp.push_back(NULL);
	_tmp.back() = xr_strdup("renderer_r3");
	u32 _cnt = _tmp.size() + 1;
	vid_quality_token = xr_alloc<xr_token>(_cnt);

	vid_quality_token[_cnt - 1].id = -1;
	vid_quality_token[_cnt - 1].name = NULL;

#ifdef DEBUG
	Msg("Available render modes[%d]:", _tmp.size());
#endif // DEBUG
	for (u32 i = 0; i < _tmp.size(); ++i)
	{
		vid_quality_token[i].id = i;
		vid_quality_token[i].name = _tmp[i];
#ifdef DEBUG
		Msg("[%s]", _tmp[i]);
#endif // DEBUG
	}
}