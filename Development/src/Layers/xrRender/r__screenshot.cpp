#include "stdafx.h"
//#include "../../xrEngine/xr_effgamma.h"
#include "xr_effgamma.h"
#include "dxRenderDeviceRender.h"
#include "../xrRender/tga.h"
#include "../../xrEngine/xrImage_Resampler.h"

#include "d3dx10tex.h"

#define	GAMESAVE_SIZE	128

IC u32 convert(float c)	{
	u32 C = iFloor(c);
	if (C > 255) C = 255;
	return C;
}
IC void MouseRayFromPoint(Fvector& direction, int x, int y, Fmatrix& m_CamMat)
{
	int halfwidth = Device.dwWidth / 2;
	int halfheight = Device.dwHeight / 2;

	Ivector2 point2;
	point2.set(x - halfwidth, halfheight - y);

	float size_y = VIEWPORT_NEAR * tanf(deg2rad(60.f) * 0.5f);
	float size_x = size_y / (Device.fHeight_2 / Device.fWidth_2);

	float r_pt = float(point2.x) * size_x / (float)halfwidth;
	float u_pt = float(point2.y) * size_y / (float)halfheight;

	direction.mul(m_CamMat.k, VIEWPORT_NEAR);
	direction.mad(direction, m_CamMat.j, u_pt);
	direction.mad(direction, m_CamMat.i, r_pt);
	direction.normalize();
}

void CRender::Screenshot(IRender_interface::ScreenshotMode mode, LPCSTR name)
{
	ID3D10Resource		*pSrcTexture;
	HW.pBaseRT->GetResource(&pSrcTexture);

	VERIFY(pSrcTexture);

	// Save
	switch (mode)
	{
	case IRender_interface::SM_FOR_GAMESAVE:
	{
		ID3D10Texture2D		*pSrcSmallTexture;

		D3D10_TEXTURE2D_DESC desc;
		ZeroMemory(&desc, sizeof(desc));
		desc.Width = GAMESAVE_SIZE;
		desc.Height = GAMESAVE_SIZE;
		desc.MipLevels = 1;
		desc.ArraySize = 1;
		desc.Format = DXGI_FORMAT_BC1_UNORM;
		desc.SampleDesc.Count = 1;
		desc.Usage = D3D10_USAGE_DEFAULT;
		desc.BindFlags = D3D10_BIND_SHADER_RESOURCE;
		CHK_DX(HW.pDevice->CreateTexture2D(&desc, NULL, &pSrcSmallTexture));

		//	D3DX10_TEXTURE_LOAD_INFO *pLoadInfo

		CHK_DX(D3DX10LoadTextureFromTexture(pSrcTexture,
			NULL, pSrcSmallTexture));

		// save (logical & physical)
		ID3DBlob*		saved = 0;
		HRESULT hr = D3DX10SaveTextureToMemory(pSrcSmallTexture, D3DX10_IFF_DDS, &saved, 0);
		//HRESULT hr					= D3DXSaveTextureToFileInMemory (&saved,D3DXIFF_DDS,texture,0);
		if (hr == D3D_OK)
		{
			IWriter*			fs = FS.w_open(name);
			if (fs)
			{
				fs->w(saved->GetBufferPointer(), (u32)saved->GetBufferSize());
				FS.w_close(fs);
			}
		}
		_RELEASE(saved);

		// cleanup
		_RELEASE(pSrcSmallTexture);
	}
		break;
	case IRender_interface::SM_NORMAL:
	{
		string64			t_stemp;
		string_path			buf;
		sprintf_s(buf, sizeof(buf), "ss_%s_%s_(%s).jpg", Core.UserName, timestamp(t_stemp), (g_pGameLevel) ? g_pGameLevel->name().c_str() : "mainmenu");
		ID3DBlob			*saved = 0;
		CHK_DX(D3DX10SaveTextureToMemory(pSrcTexture, D3DX10_IFF_JPG, &saved, 0));
		IWriter*		fs = FS.w_open("$screenshots$", buf); R_ASSERT(fs);
		fs->w(saved->GetBufferPointer(), (u32)saved->GetBufferSize());
		FS.w_close(fs);
		_RELEASE(saved);

		if (strstr(Core.Params, "-ss_tga"))
		{ // hq
			sprintf_s(buf, sizeof(buf), "ssq_%s_%s_(%s).tga", Core.UserName, timestamp(t_stemp), (g_pGameLevel) ? g_pGameLevel->name().c_str() : "mainmenu");
			ID3DBlob*		saved = 0;
			CHK_DX(D3DX10SaveTextureToMemory(pSrcTexture, D3DX10_IFF_BMP, &saved, 0));
			//		CHK_DX				(D3DXSaveSurfaceToFileInMemory (&saved,D3DXIFF_TGA,pFB,0,0));
			IWriter*		fs = FS.w_open("$screenshots$", buf); R_ASSERT(fs);
			fs->w(saved->GetBufferPointer(), (u32)saved->GetBufferSize());
			FS.w_close(fs);
			_RELEASE(saved);
		}
	}
		break;
	}

	_RELEASE(pSrcTexture);
}
